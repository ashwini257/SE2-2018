#include "stack.h"

// Function to add an item to stack.  It increases top by 1

int isFull(struct Stack* stack)
{   return stack->top == stack->capacity - 1; }


void push(struct Stack* stack, int item)
{
  if (isFull(stack))
    return;
  stack->array[++stack->top] = item;
  printf("%d pushed to stack\n", item);
}
 
